import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Persistence {
	
	void saveToFile(String fileName, Journal journal) {
		try(PrintWriter printWriter = new PrintWriter(fileName)) {
			printWriter.print(journal.toString());
			System.out.println("Entries Saved to the File");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
}
