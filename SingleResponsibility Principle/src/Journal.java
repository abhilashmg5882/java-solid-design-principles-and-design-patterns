import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Journal {
	List<String> entries = new ArrayList<String>();

	void addEntry(String journalName) {
		entries.add(journalName);
	}

	void removeEntry(String journalName) {
		entries.remove(journalName);
	}

//	void saveToFile(String fileName) {
////		PrintWriter printWriter = null;
//		try (PrintWriter printWriter = new PrintWriter(fileName);) {
////			printWriter = new PrintWriter(fileName);
//			printWriter.print(toString());
//			System.out.println("Journals saved to the file");
////			printWriter.close();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
////		finally {
////			if(printWriter != null)
////				printWriter.close();
////		}
//		
//	}

	@Override
	public String toString() {
		return String.join(System.lineSeparator(), entries);
	}

}
