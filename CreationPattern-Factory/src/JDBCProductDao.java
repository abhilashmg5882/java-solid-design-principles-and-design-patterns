
public class JDBCProductDao implements ProductDAO {

	@Override
	public void addProduct() {
		System.out.println("Adding Product by jdbc logic");
	}

	@Override
	public void deleteProduct() {
		System.out.println("Deleting the Product by jdbc logic");
	}
}
