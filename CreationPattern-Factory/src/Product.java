
public class Product {
	String productName;
	int productCost;

	public Product(String productName, int productCost) {
		super();
		this.productName = productName;
		this.productCost = productCost;
	}

}
