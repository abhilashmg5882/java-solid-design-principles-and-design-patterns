
public class ProductDAOFactory {
	ProductDAO createInstance(String ormType) {
		switch(ormType) {
		case "jdbc" :
			return new JDBCProductDao();
		case "jpa":
			return new JPAProductDao();
		default :
			throw new RuntimeException("No such orm type");
		}
	}
	
	public static void main(String[] args) {
		ProductDAOFactory daoFactory = new ProductDAOFactory();
		ProductDAO dao =  daoFactory.createInstance("jpa");
		dao.addProduct();
		dao.deleteProduct();
	}
	
}
