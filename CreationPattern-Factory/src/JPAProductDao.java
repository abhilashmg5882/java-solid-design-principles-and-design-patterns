
public class JPAProductDao implements ProductDAO {

	@Override
	public void addProduct() {
		System.out.println("Adding product by JPA logic");
	}

	@Override
	public void deleteProduct() {
		System.out.println("Deleting product by JPA Logic");
	}

}
