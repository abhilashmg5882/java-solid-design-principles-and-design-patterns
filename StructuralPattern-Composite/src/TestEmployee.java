
public class TestEmployee {
	public static void main(String[] args) {
		Employee ceo = new Employee("Umesh");
		
		Employee developementManager = new Employee("Senthil");
		Employee testingManager = new Employee("Sunil");
		
		ceo.subordinates.add(developementManager);
		ceo.subordinates.add(testingManager);
		
		Employee developer = new Employee("Prem");
		developementManager.subordinates.add(developer);
		
		Employee tester = new Employee("Midun");
		testingManager.subordinates.add(tester);
		
		for(Employee manager: ceo.subordinates) {
			System.out.println(manager.name);
			for(Employee worker: manager.subordinates) {
				System.err.println(worker.name);
			}
		}
		
	}
	

}
