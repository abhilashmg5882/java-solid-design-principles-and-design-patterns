
public class TestObserver {
	public static void main(String[] args) {
		Subject subject =new  Subject();
		
		BinaryObserver binaryObserver = new BinaryObserver(subject);
		subject.setState(10);
		
		subject.setState(100);
	}

}
