import java.util.List;

public abstract class  Observer {
	Subject subject;
	
	abstract void update();
}
