
public class RectangleFactory {
	
	public static Rectangle newRectangle(int heigth, int width) {
		return new Rectangle(heigth, width);
	}
	
	public static Rectangle newSquare(int size) {
		return new Rectangle(size, size);
	}
	
}
