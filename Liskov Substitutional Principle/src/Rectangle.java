
public class Rectangle {
	private int height;
	private int width;

	public Rectangle() {
	}

	public Rectangle(int height, int width) {
		super();
		this.height = height;
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void area() {
		System.out.println("Area is = " + (width * height));
	}

}
