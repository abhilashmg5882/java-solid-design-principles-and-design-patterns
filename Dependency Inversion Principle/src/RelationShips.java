import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class RelationShips implements RelationShipsDao { //Low Level Module
	List<Triple> relations = new ArrayList<Triple>();
	
	
	void addParentAndChild(Person person1, Person person2) {
		relations.add(new Triple(person1, RelationShip.PARENT, person2));
		relations.add(new Triple(person2, RelationShip.CHILD, person1));
	}


	@Override
	public Stream<Triple> findChildren(String parentName) {
		return this.relations.stream().filter(triple -> {
			return triple.person1.name.equals(parentName) && triple.relationShip.equals(RelationShip.PARENT);
		});
	}
}
