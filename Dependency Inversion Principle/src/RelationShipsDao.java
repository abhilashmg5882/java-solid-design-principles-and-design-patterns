import java.util.stream.Stream;

public interface RelationShipsDao {
	
	public Stream<Triple> findChildren(String parentName);
	
}
