
public class Triple {
	Person person1;
	RelationShip relationShip;
	Person person2;

	public Triple(Person person1, RelationShip relationShip, Person person2) {
		super();
		this.person1 = person1;
		this.relationShip = relationShip;
		this.person2 = person2;
	}

}
