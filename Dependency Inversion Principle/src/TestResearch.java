
public class TestResearch {
	public static void main(String[] args) {
		RelationShips relationShips = new RelationShips();
		
		relationShips.addParentAndChild(new Person("John"), new Person("Martin"));
		relationShips.addParentAndChild(new Person("John"), new Person("Ramu"));
		
		Research research = new Research();
		research.findChildrens("John", relationShips);
	}

}
