
public class Research { //high level module
	public void findChildrens(String parentName, RelationShipsDao relationShips) { //"John"
//		relationShips.relations.stream().filter(triple -> {
//			return triple.person1.name.equals(parentName) && triple.relationShip.equals(RelationShip.PARENT);
//		}).forEach(triple -> {
//			System.out.println(parentName + " has a child "+triple.person2.name);
//		});
		relationShips.findChildren(parentName).forEach(triple -> {
			System.out.println(parentName + " has a child "+triple.person2.name);
		});
	}
}
