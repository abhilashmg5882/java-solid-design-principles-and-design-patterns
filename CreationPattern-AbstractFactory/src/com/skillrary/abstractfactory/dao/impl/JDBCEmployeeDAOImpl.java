package com.skillrary.abstractfactory.dao.impl;

import com.skillrary.abstractfactory.dao.EmployeeDAO;

public class JDBCEmployeeDAOImpl implements EmployeeDAO {

	@Override
	public void addEmploye() {
		System.out.println("Adding employee by jdbc logic");
	}

	@Override
	public void updateEmployee() {
		System.out.println("updating employee by jdbc logic");
	}
	
}
