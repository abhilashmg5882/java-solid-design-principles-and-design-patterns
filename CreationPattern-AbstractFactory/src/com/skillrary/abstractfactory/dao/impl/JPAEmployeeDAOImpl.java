package com.skillrary.abstractfactory.dao.impl;

import com.skillrary.abstractfactory.dao.EmployeeDAO;

public class JPAEmployeeDAOImpl implements EmployeeDAO {

	@Override
	public void addEmploye() {
		System.out.println("Adding Employee by JPA logic");
	}

	@Override
	public void updateEmployee() {
		System.out.println("Updating Employee by JPA logic");
	}

}
