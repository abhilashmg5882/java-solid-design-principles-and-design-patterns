package com.skillrary.abstractfactory.dao.impl;

import com.skillrary.abstractfactory.dao.EmployeeDAO;
import com.skillrary.abstractfactory.dao.ProductDAO;

public class JDBCProductDAOImpl implements ProductDAO {

	@Override
	public void addProduct() {
		System.out.println("Adding product by jdbc logic");
	}

	@Override
	public void updateProduct() {
		System.out.println("updating product by jdbc logic");
	}
	
}
