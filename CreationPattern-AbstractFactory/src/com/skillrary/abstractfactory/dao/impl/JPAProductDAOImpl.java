package com.skillrary.abstractfactory.dao.impl;

import com.skillrary.abstractfactory.dao.EmployeeDAO;
import com.skillrary.abstractfactory.dao.ProductDAO;

public class JPAProductDAOImpl implements ProductDAO {

	@Override
	public void addProduct() {
		System.out.println("Adding Product by JPA logic");
	}

	@Override
	public void updateProduct() {
		System.out.println("Updating Product by JPA logic");
	}

}
