package com.skillrary.abstractfactory.dao;

public interface ProductDAO {
	public void addProduct();
	
	public void updateProduct();
}
