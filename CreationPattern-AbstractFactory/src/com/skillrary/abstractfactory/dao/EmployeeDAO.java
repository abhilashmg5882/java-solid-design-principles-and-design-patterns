package com.skillrary.abstractfactory.dao;

public interface EmployeeDAO {
	
	public void addEmploye();
	
	public void updateEmployee();
}
