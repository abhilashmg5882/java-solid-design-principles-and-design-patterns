package com.skillrary.abstractfactory;

import com.skillrary.abstractfactory.dao.EmployeeDAO;
import com.skillrary.abstractfactory.dao.ProductDAO;

public class ApplicationTest {
	
	static AbstractFactory getAbstractFactory(String ormType) {
		switch(ormType) {
		case "jdbc": 
			return new JDBCAbstractFactory();
		case "jpa": 
			return new JPAAbstractFactory();
		default: 
			throw new RuntimeException("No Such orm Type");
		}
	}
	
	
	public static void main(String[] args) {
		AbstractFactory abstractFactory = getAbstractFactory("jdbc");
		EmployeeDAO employeeDAO = abstractFactory.getEmployeeDAO();
		ProductDAO productDAO = abstractFactory.getProductDao();
		employeeDAO.addEmploye();
		employeeDAO.updateEmployee();
		productDAO.addProduct();
		productDAO.updateProduct();
	}
}
