package com.skillrary.abstractfactory;

import com.skillrary.abstractfactory.dao.EmployeeDAO;
import com.skillrary.abstractfactory.dao.ProductDAO;

public interface AbstractFactory {
	
	public ProductDAO getProductDao();
	
	public EmployeeDAO getEmployeeDAO();

}
