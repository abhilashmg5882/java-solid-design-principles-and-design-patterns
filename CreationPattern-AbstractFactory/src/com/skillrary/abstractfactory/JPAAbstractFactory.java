package com.skillrary.abstractfactory;

import com.skillrary.abstractfactory.dao.EmployeeDAO;
import com.skillrary.abstractfactory.dao.ProductDAO;
import com.skillrary.abstractfactory.dao.impl.JPAEmployeeDAOImpl;
import com.skillrary.abstractfactory.dao.impl.JPAProductDAOImpl;

public class JPAAbstractFactory implements AbstractFactory {

	@Override
	public ProductDAO getProductDao() {
		return new JPAProductDAOImpl();
	}

	@Override
	public EmployeeDAO getEmployeeDAO() {
		// TODO Auto-generated method stub
		return new JPAEmployeeDAOImpl();
	}

}
