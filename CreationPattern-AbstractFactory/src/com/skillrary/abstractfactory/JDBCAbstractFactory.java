package com.skillrary.abstractfactory;

import com.skillrary.abstractfactory.dao.EmployeeDAO;
import com.skillrary.abstractfactory.dao.ProductDAO;
import com.skillrary.abstractfactory.dao.impl.JDBCEmployeeDAOImpl;
import com.skillrary.abstractfactory.dao.impl.JDBCProductDAOImpl;

public class JDBCAbstractFactory implements AbstractFactory {

	@Override
	public ProductDAO getProductDao() {
		return new JDBCProductDAOImpl();
	}

	@Override
	public EmployeeDAO getEmployeeDAO() {
		return new JDBCEmployeeDAOImpl();
	}

}
