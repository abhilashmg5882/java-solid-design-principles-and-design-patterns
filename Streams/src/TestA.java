import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestA {
	public static void main(String[] args) {
		
		List<Integer> l = new ArrayList<Integer>();
		l.add(10);
		l.add(3);
		l.add(5);
		l.add(20);
		l.add(27);
		l.add(98);
		l.add(67);
		
		System.out.println(l);
		
		l.stream().filter(num -> (num % 2 == 0)).forEach(num -> {
			System.out.println(num);
		});
		
		List<Integer> newArray = l.stream().map(num -> num * 100).collect(Collectors.toList());
		for(Integer num: newArray) {
			System.out.println(num);
		}
	}

}
