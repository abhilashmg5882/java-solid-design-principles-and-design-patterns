import java.util.TreeSet;

public class TestStudent {
	public static void main(String[] args) {
		
		TreeSet<Student> students = new TreeSet<Student>(new SortByStudentAge());
		
		students.add(new Student("Rakesh", 26));
		students.add(new Student("Harsha", 25));
		students.add(new Student("Abhi", 28));
		
		
		System.out.println(students);
		
	}

}
