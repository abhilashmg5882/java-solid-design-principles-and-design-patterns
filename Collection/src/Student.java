
public class Student {// implements Comparable<Student> {
	String studName;
	Integer studAge;

	public Student(String studName, Integer studAge) {
		super();
		this.studName = studName;
		this.studAge = studAge;
	}

//	@Override
//	public int compareTo(Student o) {
//		if (this.studAge > o.studAge) {
//			return 1;
//		} else if (this.studAge < o.studAge) {
//			return -1;
//		}
//		return 0;
//	}
	
	

	@Override
	public String toString() {
		return "Student [studName=" + studName + ", studAge=" + studAge + "]";
	}

//	@Override
//	public int compareTo(Student o) {
//		return this.studName.compareTo(o.studName);
//	}

}
