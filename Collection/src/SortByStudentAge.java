import java.util.Comparator;

public class SortByStudentAge implements Comparator<Student>{

	@Override
	public int compare(Student o1, Student o2) {
		return o1.studAge.compareTo(o2.studAge);
	}

}
