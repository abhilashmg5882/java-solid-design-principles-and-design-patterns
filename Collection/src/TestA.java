import java.util.TreeSet;

public class TestA {
	public static void main(String[] args) {
		TreeSet<Integer> t = new TreeSet<Integer>();
		t.add(10); //int value or Integer object 
		//t1.add(Integer.valueOf(10)); autoboxing
		t.add(20);
		t.add(98);
		t.add(8);
		t.add(6);
		
		System.out.println(t);
		
	}
}
