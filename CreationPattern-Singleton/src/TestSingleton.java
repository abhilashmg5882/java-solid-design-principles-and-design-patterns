
public class TestSingleton {
	public static void main(String[] args) {
		
		Employee employee = Employee.getInstance();
		employee.setEmpId(122);
		employee.setEmpName("Harsha");
		
		employee.setEmpId(133);
		System.out.println(employee);
		
		Employee employee1 = Employee.getInstance();
		System.out.println(employee1);
	}

}
