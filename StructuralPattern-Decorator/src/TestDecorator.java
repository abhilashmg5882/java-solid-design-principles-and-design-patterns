
public class TestDecorator {
	public static void main(String[] args) {
		DecorateRectangle decorateRectangle = new DecorateRectangle();
		decorateRectangle.rectangle = new Rectangle(); 
		decorateRectangle.rectangle.shape();

		decorateRectangle.borderColor();
	}
}
