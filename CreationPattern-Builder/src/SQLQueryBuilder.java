
public class SQLQueryBuilder {
	StringBuilder sb = new StringBuilder();
	boolean isConditionSpecified = false;
	
	
	public SQLQueryBuilder fromClause(String tableName) {
		sb.append("select * from "+tableName);
		return this;
	}
	
	public SQLQueryBuilder whereAndClause(String condition, String value) {
		sb.append(isConditionSpecified? " and ": " where ")
		.append(condition).append(value);
		isConditionSpecified = true;
		return this;
	}

	@Override
	public String toString() {
		return sb.toString();
	}

}
