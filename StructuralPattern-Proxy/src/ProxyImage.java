
public class ProxyImage {
	RealImage image;
	
	
	void display() {
		if(image == null ) {
			image = new RealImage();
		}
		image.display();
	}
}
