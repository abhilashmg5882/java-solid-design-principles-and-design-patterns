
public class NameConatiner implements Container {
	String[] names = { "Ramu", "Harsha", "Rakesh" };
	Iterator iterator = new NameIterator();

	class NameIterator implements Iterator {
		int index;

		@Override
		public boolean hasNext() {
			if (index < names.length) {
				return true;
			}
			return false;
		}

		@Override
		public Object next() {
			if (this.hasNext()) {
				return names[index++];
			}
			return null;
		}

	}

	public Iterator getIterator() {
		return iterator;
	}
}
