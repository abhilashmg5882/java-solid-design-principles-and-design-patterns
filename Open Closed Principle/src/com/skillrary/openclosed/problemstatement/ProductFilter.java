package com.skillrary.openclosed.problemstatement;

import java.util.List;
import java.util.stream.Stream;

public class ProductFilter {
	
	Stream<Product> filterByColor(List<Product> products, Color color) {
		return products.stream().filter(product -> product.color.equals(color));
	}
	
	Stream<Product> filterBySize(List<Product> products, Size size) {
		return products.stream().filter(product -> product.size.equals(size));
	}
}
