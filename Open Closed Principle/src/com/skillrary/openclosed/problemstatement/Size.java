package com.skillrary.openclosed.problemstatement;

public enum Size {
	SMALL, 
	MEDIUM,
	LARGE
}
