package com.skillrary.openclosed.problemstatement;

import java.util.ArrayList;
import java.util.List;

public class TestProducts {
	public static void main(String[] args) {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("TShirt", Color.GREEN, Size.LARGE));
		products.add(new Product("Jeans", Color.RED, Size.SMALL));
		
		ProductFilter filter = new ProductFilter();
		filter.filterByColor(products, Color.RED).forEach(product -> {
			System.out.println(product);
		});
		
		filter.filterBySize(products, Size.LARGE).forEach(product -> {
			System.out.println(product);
		});
			
	}

}
