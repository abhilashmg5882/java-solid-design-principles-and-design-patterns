package com.skillrary.openclosed.problemstatement;

public class Product {
	public String productName;
	public Color color;
	public Size size;

	public Product(String productName, Color color, Size size) {
		super();
		this.productName = productName;
		this.color = color;
		this.size = size;
	}

	@Override
	public String toString() {
		return "Product [productName=" + productName + ", color=" + color + ", size=" + size + "]";
	}

}
