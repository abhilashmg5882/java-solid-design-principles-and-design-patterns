package com.skillrary.openclosed.solution;

import java.util.ArrayList;
import java.util.List;

import com.skillrary.openclosed.problemstatement.Color;
import com.skillrary.openclosed.problemstatement.Product;
import com.skillrary.openclosed.problemstatement.Size;

public class TestNewProductFilter {
	public static void main(String[] args) {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("TShirt", Color.GREEN, Size.LARGE));
		products.add(new Product("Jeans", Color.RED, Size.SMALL));
		
		NewProductFilter filter = new NewProductFilter();
		System.out.println("filtering based on color");
		filter.filterProducts(products, new ColorSpecification(Color.GREEN)).forEach(product -> {
			System.out.println(product);
		});
		System.out.println("filtering based on size");
		filter.filterProducts(products, new SizeSpecification(Size.SMALL)).forEach(product -> {
			System.out.println(product);
		});
	}

}
