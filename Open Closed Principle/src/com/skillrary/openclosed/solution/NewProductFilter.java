package com.skillrary.openclosed.solution;

import java.util.List;
import java.util.stream.Stream;

import com.skillrary.openclosed.problemstatement.Product;

public class NewProductFilter {
	
	public Stream<Product> filterProducts(List<Product> products,Specification specification) {
		return products.stream().filter(product -> specification.isSatisified(product));
	}
}
