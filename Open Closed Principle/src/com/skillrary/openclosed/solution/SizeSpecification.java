package com.skillrary.openclosed.solution;

import com.skillrary.openclosed.problemstatement.Product;
import com.skillrary.openclosed.problemstatement.Size;

public class SizeSpecification implements Specification {
	Size size;

	public SizeSpecification(Size size) {
		super();
		this.size = size;
	}

	@Override
	public boolean isSatisified(Product product) {
		return product.size.equals(size);
	}
}
