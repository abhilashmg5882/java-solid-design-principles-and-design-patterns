package com.skillrary.openclosed.solution;

import com.skillrary.openclosed.problemstatement.Product;

public interface Specification {
	
	boolean isSatisified(Product product);
	
}
