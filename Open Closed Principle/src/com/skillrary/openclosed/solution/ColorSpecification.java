package com.skillrary.openclosed.solution;

import com.skillrary.openclosed.problemstatement.Color;
import com.skillrary.openclosed.problemstatement.Product;

public class ColorSpecification implements Specification {
	Color color;

	public ColorSpecification(Color color) {
		super();
		this.color = color;
	}

	@Override
	public boolean isSatisified(Product product) {
		return product.color.equals(color);
	}
}
