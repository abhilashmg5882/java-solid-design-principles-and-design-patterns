
public interface Machine {
	
	void print(Document document);
	
	void scan(Document document) throws Exception;
	
	void fax(Document document);
}
